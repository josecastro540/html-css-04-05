$(document).ready(function(){
	$('.carousel').carousel({
		interval: 4000,
		wrap: false
	});

	 $('[data-toggle="tooltip"]').tooltip();

	  $('[data-toggle="popover"]').click(function(e){
	 	e.preventDefault();
	 	 $('[data-toggle="popover"]').popover();
	 })
});